angular.module('myApp', ['ngRoute', 'toaster', 'ng-mfb', 'ui.bootstrap-slider', 'ui.bootstrap', 'isteven-multi-select', 'googlechart', 'ngAnimate', 'ui.sortable'])
    .config(['$routeProvider', function($routeProvider) {

        $routeProvider
            .when('/home', {
                templateUrl: 'src/views/home.html',
                controller: 'mainCtr'
            })
            .when("/search", {
                templateUrl: 'src/views/results.html',
                controller: "mainCtr"
            })
            .when('/detail/:id', {
                templateUrl: 'src/views/detail.html',
                controller: 'detailCtr'
            })
            .when('/stats', {
                templateUrl: 'src/views/stats.html',
                controller: 'statsCtr'
            })
            .otherwise({
                redirectTo: '/home'
            })

    }]).run(['$rootScope', 'dashboardSrv', '$http', function($rootScope, dashboardSrv, $http) {

        $http.get("http://localhost:9000/dboard/db/1").success(function(res) {
            $rootScope.dash = res;
            console.log(res._source);
            $rootScope.dash.ready = true;
        }).error(function(res) {
            $rootScope.dash = [];
            $rootScope.dash.ready = false;
            $http.post("http://localhost:9000/dboard").success(function(res) {
                var data = {
                    "mappings": {
                        "db": {
                            "_timestamp": {
                                "enabled": "true",
                                "store": "yes"
                            }
                        }
                    }
                }
            })

        })


        $rootScope.charts2 = [];
    }])


var myapp = angular.module('myApp');


myapp.service('querySrv', [function() {
    var myQuery = [];
    var mustFilter = [];
    var mustNotFilter = [];
    var shouldFilter = [];
    var sortByFilter = [];
    var aggsFilter = [];
    return {
        generateFilter: function(filter_type, query_type, field, dt1, dt2) {
            if (filter_type == "must") {

                if (query_type == "term") {
                    if (dt1.length > 0) {
                        for (var i = 0; i < dt1.length; i++) {
                            var str = "{\"" + query_type + "\":{\"" + field + "\":\"" + dt1[i].name + "\"}}";
                            mustFilter.push(JSON.parse(str));
                        }
                    }
                } else if (query_type == "range") {
                    var str = "{\"" + query_type + "\":{\"" + field + "\":{\"gte\":" + dt1 + ",\"lte\":" + dt2 + "}}}";
                    mustFilter.push(JSON.parse(str));
                }


            } else if (filter_type == "should") {
                if (query_type == "term") {
                    if (dt1.length > 0) {
                        for (var i = 0; i < dt1.length; i++) {
                            var str = "{\"" + query_type + "\":{\"" + field + "\":\"" + dt1[i].name + "\"}}";
                            shouldFilter.push(JSON.parse(str));
                        }
                    }
                } else if (query_type == "range") {
                    var str = "{\"" + query_type + "\":{\"" + field + "\":{\"gte\":" + dt1 + ",\"lte\":" + dt2 + "}}}";
                    shouldFilter.push(JSON.parse(str));
                }
            } else if (filter_type == "must_not") {
                if (query_type == "term") {
                    if (dt1.length > 0) {
                        for (var i = 0; i < dt1.length; i++) {
                            var str = "{\"" + query_type + "\":{\"" + field + "\":\"" + dt1[i].name + "\"}}";
                            mustNotFilter.push(JSON.parse(str));
                        }
                    }
                } else if (query_type == "range") {
                    var str = "{\"" + query_type + "\":{\"" + field + "\":{\"gte\":" + dt1 + ",\"lte\":" + dt2 + "}}}";
                    mustNotFilter.push(JSON.parse(str));
                }
            } else if (filter_type == "sort") {
                var str = "{\"" + field + "\":{\"order\":\"" + dt1 + "\",\"mode\":\"" + dt2 + "\"}}";
                sortByFilter.push(JSON.parse(str));
            } else if (filter_type == "aggs") {
                var str = "{\"" + query_type + "\":{\"terms\":{\"field\":\"" + field + "\"}}}";
                aggsFilter.push(JSON.parse(str));
            } else if (filter_type == "hist") {
                var str = "{\"" + query_type + "\":{\"histogram\":{\"field\":\"" + field + "\",\"interval\":" + dt1 + "}}}";
                aggsFilter.push(JSON.parse(str));
            }

        },
        generateQuery: function(filter, data, pg, sz) {
            myQuery = [];
            var aggs;
            if (aggsFilter.length > 0) {
                aggs = aggsFilter[0]
            } else {
                aggs = {}
            }
            if (filter) {
                myQuery.push({

                    "query": {
                        "match_all": {

                        }
                    },
                    "from": pg,
                    "size": sz,
                    "sort": sortByFilter,
                    "filter": {
                        "bool": {
                            "must": mustFilter,
                            "must_not": mustNotFilter,
                            "should": shouldFilter

                        }
                    },
                    "aggs": aggs
                })
            } else {
                myQuery.push({

                    "query": {
                        "match": {
                            "_all": {
                                "query": data,
                                "operator": "and"
                            }
                        }
                    },
                    "from": pg,
                    "size": 50,
                    "sort": sortByFilter,
                    "filter": {
                        "bool": {
                            "must": mustFilter,
                            "must_not": mustNotFilter,
                            "should": shouldFilter

                        }
                    },
                    "aggs": aggs
                })
            }

        },
        getQuery: function() {
            return myQuery;
        },
        resetSortFilter: function() {
            sortByFilter = [];
        },
        resetQuery: function() {
            myQuery = [];
            mustFilter = [];
            mustNotFilter = [];
            shouldFilter = [];
            sortByFilter = [];
            aggsFilter = [];
        }
    }
}]);

myapp.service('client', ['$http', 'querySrv', '$rootScope', function($http, querySrv, $rootScope) {

    return {
        search: function(callback) {
            $rootScope.serviceActive = true;
            $http.post("http://localhost:9000/imdb/_search", querySrv.getQuery()[0]).success(function(res) {
                callback(res);
                $rootScope.serviceActive = false;
            })
        }
    }

}])



angular.module('myApp').service('chartSrv', function() {
    return {
        generateCol: function(arg, arg2) {
            arg.push(arg2);
        },
        generateRow: function(arg, arg2) {
            arg.push(arg2);
        },
        generateChart: function(chart1, arg, cType, cStyle, cTtt, hTtt, vTtt) {
            chartType = cType;
            cssStyle = cStyle;
            cTitle = cTtt;
            hTitle = hTtt;
            vTitle = vTtt;
            chart1.type = chartType;
            chart1.cssStyle = cssStyle;
            chart1.data = arg;
            chart1.options = {
                "title": cTitle,
                "isStacked": true,
                "fill": 20,
                "displayExactValues": true,
                "vAxis": {
                    "title": vTitle,
                    "gridlines": {
                        "count": 6
                    }
                },
                "hAxis": {
                    "title": hTitle,

                }
            };

            chart1.formatters = {};

            return chart1;
        }


    }

})


angular.module('myApp').service('chartGenSrv', ['client', 'querySrv', 'chartSrv', function(client, querySrv, chartSrv) {
    return {
        generate: function(aggs_type, interval, col, label, field, 
            chart, chartType, cssStyle, cTitle, vTitle, hTitle, args,callback) {
            querySrv.resetQuery();
            chartSrv.generateCol(col, ["Country", label]);
            querySrv.generateFilter(aggs_type, "tag", field, interval, "");
            querySrv.generateQuery(true, "", 0, 0);
            client.search(function(res) {
                for (var i = 0; i < res.aggregations.tag.buckets.length; i++) {
                    if (res.aggregations.tag.buckets[i].key.toString() !== "") {
                        chartSrv.generateRow(col, [res.aggregations.tag.buckets[i].key.toString(),
                            parseInt(res.aggregations.tag.buckets[i].doc_count)
                        ]);
                    }

                };

                querySrv.resetQuery();
                callback(chartSrv.generateChart(chart, col, chartType, cssStyle, cTitle, vTitle, hTitle),args);
            });
        }
    }
}])


angular.module('myApp').service('dashboardSrv', ['$http', '$rootScope', function($http, $rootScope) {
    var dboard = {};

    return {
        saveDboard: function(isDefault, title) {
            var query = {
                "size": 1,
                "query": {
                    "match_all": {}
                },
                "sort": [{
                    "_timestamp": {
                        "order": "desc"
                    }
                }]
            }
            dboard = {
                "isDefault": isDefault,
                "title": title,
                "dash": $rootScope.dash
            }
            $http.get("http://localhost:9000/dboard/_search", query).success(function(res) {
                if (parseInt(res.hits.total) == 0) {
                    console.log("bla"+dboard.isDefault)
                    $http.post("http://localhost:9000/dboard/db/1", dboard).success(function(res) {})
                } else {
                    if (isDefault) {
                        console.log("bla"+dboard.isDefault)
                        $http.post("http://localhost:9000/dboard/db/1", dboard).success(function(res) {})
                    } else {
                        $http.post("http://localhost:9000/dboard/db/" + (parseInt(res.hits.hits[0]._id) + 1), dboard).success(function(res) {})
                    }
                }

            })
        },
        getDefaultDboard: function(callback) {
            $http.get("http://localhost:9000/dboard/db/1").success(function(res) {
                callback(res);
            })
        },
        getDboard: function(id, callback) {
            $http.get("http://localhost:9000/dboard/db/" + id).success(function(res) {
                callback(res);
            })
        }
    }
}])


angular.module('myApp').service('tempSrv', ['$http', function($http){
    var temp;
    return{
        setTemp:function(arg){
            temp=arg;
        },
        getTemp:function(){
            return temp;
        },
        reset:function(){
            temp=[];
        }
    }
}])

angular.module('myApp').controller('mainCtr', ['$scope', 'querySrv', 'client', function($scope, querySrv, client) {
    $scope.isCollapsed = true;
    $scope.maxSize = 5;
    $scope.bigCurrentPage = 1;
    $scope.selectedSearch = false;
    $scope.filtered = false;


    $scope.pageChange = function(bigCurrentPage) {
        if ($scope.selectedSearch) {
            querySrv.generateQuery(false, $scope.selected, (bigCurrentPage - 1) * 25, 25);
        } else {
            querySrv.generateQuery(true, "", (bigCurrentPage - 1) * 25, 25);
        }

        client.search(function(res) {
            $scope.topList = res;
            $scope.bigTotalItems = res.hits.total;
        });
    }


    //// GENRE FILTER
    var genreList = [];
    querySrv.generateFilter("aggs", "tag", "Genre", "", "");
    querySrv.generateQuery(true, "", 0, 0);
    client.search(function(res) {
        for (var i = 0; i < res.aggregations.tag.buckets.length; i++) {
            var str = "{\"name\":\"" + res.aggregations.tag.buckets[i].key + "\"}";
            genreList.push(JSON.parse(str));
        };
        $scope.genreList = genreList;
    });
    querySrv.resetQuery();
    //// LANGUAGE FILTER
    var languageList = [];
    querySrv.generateFilter("aggs", "tag", "Country", "", "");
    querySrv.generateQuery(true, "", 0, 0);
    client.search(function(res) {
        for (var i = 0; i < res.aggregations.tag.buckets.length; i++) {
            var str = "{\"name\":\"" + res.aggregations.tag.buckets[i].key + "\"}";
            languageList.push(JSON.parse(str));
        };
        $scope.languageList = languageList;
    });
    querySrv.resetQuery();
    //// COUNTRY FILTER
    var countryList = [];
    querySrv.generateFilter("aggs", "tag", "Awards", "", "");
    querySrv.generateQuery(true, "", 0, 0);
    client.search(function(res) {
        for (var i = 0; i < res.aggregations.tag.buckets.length; i++) {
            var str = "{\"name\":\"" + res.aggregations.tag.buckets[i].key + "\"}";
            countryList.push(JSON.parse(str));
        };
        $scope.countryList = countryList;
    });
    querySrv.resetQuery();

    $scope.saveFilter = function() {
        $scope.bigCurrentPage = 1;
        querySrv.resetQuery();
        $scope.isCollapsed = true;
        $scope.filtered = true;
        querySrv.generateFilter("must", "range", "imdbVotes", $scope.imdb[0], $scope.imdb[1]);
        querySrv.generateFilter("must", "range", "Year", $scope.year[0], $scope.year[1]);
        querySrv.generateFilter("must", "term", "Genre", $scope.selectedGenre, "");
        querySrv.generateFilter("should", "term", "Awards", $scope.selectedCountry, "");
        querySrv.generateFilter("should", "term", "Country", $scope.selectedLanguage, "");
        if ($scope.selectedSearch) {
            querySrv.generateQuery(false, $scope.selected, ($scope.bigCurrentPage - 1) * 25, 25);
        } else {
            querySrv.generateQuery(true, "", ($scope.bigCurrentPage - 1) * 25, 25);
        }
        client.search(function(res) {
            $scope.topList = res;
            $scope.bigTotalItems = res.hits.total;
        });

    }

    $scope.resetFilter = function() {
        $scope.isCollapsed = true;
        $scope.filtered = false;
        querySrv.resetQuery();
        $scope.selectedCountry = [];
        $scope.selectedLanguage = [];
        $scope.selectedGenre = [];
        $scope.imdb = [0, 10];
        $scope.year = [1900, 2016];
    }


    querySrv.generateFilter("must", "range", "Poster", 1000, 7000);
    querySrv.generateFilter("sort", "", "imdbVotes", "desc", "max");
    querySrv.generateQuery(true, "", 0, 25);
    client.search(function(res) {
        $scope.topList = res;
        $scope.bigTotalItems = res.hits.total;
    });
    $scope.sortType = "desc";
    $scope.toggleSort = function() {
        if ($scope.sortType == "desc") {
            $scope.sortType = "asc";
        } else {
            $scope.sortType = "desc";
        }
    }
    $scope.sortBy = function(arg) {
        if ($scope.filtered && $scope.selectedSearch) {
            $scope.filtered = true;
            querySrv.resetSortFilter();
            querySrv.generateFilter("sort", "", arg, $scope.sortType, "avg");
            querySrv.generateQuery(false, $scope.selected, ($scope.bigCurrentPage - 1) * 25, 25);
        } else if ($scope.filtered && !$scope.selectedSearch) {
            querySrv.resetSortFilter();
            querySrv.generateFilter("sort", "", arg, $scope.sortType, "avg");
            querySrv.generateQuery(true, "", ($scope.bigCurrentPage - 1) * 25, 25);
        } else if (!$scope.filtered && $scope.selectedSearch) {
            querySrv.resetSortFilter();
            querySrv.generateFilter("sort", "", arg, $scope.sortType, "avg");
            querySrv.generateQuery(false, $scope.selected, ($scope.bigCurrentPage - 1) * 25, 25);
        } else {
            querySrv.resetSortFilter();
            querySrv.generateFilter("sort", "", arg, $scope.sortType, "avg");
            querySrv.generateQuery(true, "", ($scope.bigCurrentPage - 1) * 25, 25);
        }
        client.search(function(res) {
            $scope.topList = res;
            $scope.bigTotalItems = res.hits.total;
        });
    }


    $scope.sub = function() {
        $scope.bigCurrentPage = 1;
        $scope.selectedSearch = true;
        if ($scope.filtered) {
            $scope.filtered = true;
        } else {
            querySrv.resetQuery();
        }
        querySrv.generateQuery(false, $scope.selected, ($scope.bigCurrentPage - 1) * 25, 25);
        client.search(function(res) {
            $scope.topList = res;
            $scope.bigTotalItems = res.hits.total;
        });
    }
    $scope.label = function(item) {
        return item.Title + ' | Director: ' + item.Director + '| Released: ' + item.Released + ' | Cast: ' + item.Metacritic;
    };
    $scope.autoF = function() {
        if ($scope.filtered) {
            $scope.filtered = true;
        } else {
            querySrv.resetQuery();
        }
        querySrv.generateQuery(false, $scope.selected, 0, 10);
        client.search(function(res) {
            $scope.autos = $.map(res.hits, function(el) {
                return el;
            });
        });
    }

    $scope.red = function(arg) {
        window.location = "#/detail/" + arg
    }

}])

angular.module('myApp').controller('detailCtr', ['$scope', '$http', '$routeParams', function($scope, $http, $routeParams) {

    $http.get("http://localhost:9200/imdb/movies/" + $routeParams.id).success(function(res) {
        $scope.result = res;
    })

}])


angular.module('myApp').controller('statsCtr', ['$scope','toaster','tempSrv', 'dashboardSrv', '$modal', '$rootScope', 'querySrv', 'chartSrv', 'client', 'chartGenSrv',
    function($scope,toaster,tempSrv, dashboardSrv, $modal, $rootScope, querySrv, chartSrv, client, chartGenSrv) {
        $scope.chartModel = "Metacritic";
        $scope.chartTyp = "BarChart";
        $scope.panelColor = "default";
        $scope.panelSize = "col-md-4";


        $scope.open = function() {
            $rootScope.t = $modal.open({
                templateUrl: 'panel.html',
                windowClass: 'modal-default',
                controller: 'statsCtr',
                size: 'lg',
            });
        }




        $scope.sortableOptions = {
            'ui-floating': true,
            handle: '.myHandle',
        }

        if ($rootScope.dash.ready) {
            $rootScope.temp=[];
            var temp = $rootScope.dash._source.dash
            tempSrv.reset();
            tempSrv.setTemp($rootScope.dash._source.dash);


            for (var i = 0; i < temp.length; i++) {
                $rootScope.dash = [];
               
                var col = [];
                var chart = {};
                chartGenSrv.generate("aggs", "",
                    col, temp[i][0], temp[i][0], chart,
                    temp[i][1], "", "", "", "",temp[i],
                    function(dt,tp) {
                   console.log("asdasd"+tp)
                        $rootScope.charts2.push([tp[0], dt,
                           tp[2],
                            tp[3],
                            tp[4]
                        ]);

                        $rootScope.dash.push([tp[0], 
                            tp[1], tp[2], 
                            tp[3], tp[4]])
                    });
            };
            $scope.dama=$rootScope.dash;
        }

        $scope.charts = $rootScope.charts2;

        $scope.saveBoard = function(title, isDefault) {
            dashboardSrv.saveDboard(true, "masa");
            toaster.pop('success', "Dashboard Saved", "");
        }

        $scope.addChart = function(field, chartType, panelSize, panelColor, panelTitle) {
            if (!panelTitle) {
                panelTitle = "Untitled Panel"
            }
            var col = [];
            var chart = {};
            chartGenSrv.generate("aggs", "", col, field, field, chart,
                chartType, "", "", "", "","",
                function(dt) {
                    $rootScope.charts2.push([field, dt, panelSize, panelColor, panelTitle]);
                    $rootScope.dash.push([field, chartType, panelSize, panelColor, panelTitle]);

                });
            $rootScope.t.close();
        }

        $scope.removeChart = function(index) {
            console.log("index" + index);
            if (index > -1) {
                $scope.charts.splice(index, 1);
                $scope.dash.splice(index, 1);
            }
        }

    }
])
