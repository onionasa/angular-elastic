module.exports = function(grunt) {
    var proxySnippet = require('grunt-connect-proxy/lib/utils').proxyRequest;
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            options: {
                separator: "\n\n"
            },
            deps: {
                src: [
                    'bower_components/angular/angular.min.js',
                    'bower_components/jquery/dist/jquery.min.js',
                    'src/js/jquery-ui-1.10.4.custom/js/jquery-ui-1.10.4.custom.min.js',
                    'bower_components/angular-google-chart/ng-google-chart.js',
                    'bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js',
                    'bower_components/angular-ui-sortable/sortable.min.js',
                    'bower_components/bootstrap/dist/js/bootstrap.min.js',
                    'bower_components/angular-route/angular-route.min.js',
                    'bower_components/angular-multi-select/isteven-multi-select.js',
                    'bower_components/seiyria-bootstrap-slider/dist/bootstrap-slider.min.js',
                    'bower_components/angular-animate/angular-animate.min.js',
                    'bower_components/angular-bootstrap-slider/slider.js',
                    'bower_components/angular-google-chart/ng-google-chart.js',
                    'bower_components/angularjs-toaster/toaster.min.js',
                    'bower_components/ng-mfb/src/mfb-directive.js'

                ],
                dest: 'src/js/<%= pkg.name %>-deps.js'
            },
            css1: {
                src: ['bower_components/angular-multi-select/isteven-multi-select.css'],
                dest: 'src/css/isteven-multi-select.css'
            },
            css2: {
                src: ['bower_components/seiyria-bootstrap-slider/dist/css/bootstrap-slider.min.css'],
                dest: 'src/css/bootstrap-slider.css'
            },
            css3: {
                src: ['bower_components/ng-mfb/mfb/dist/mfb.min.css'],
                dest: 'src/css/mfb.css'
            },
            css4: {
                src: ['bower_components/angularjs-toaster/toaster.min.css'],
                dest: 'src/css/toaster.css'
            },
            myJs: {
                src: [
                    'app.js',
                    'myjs/services.js',
                    'myjs/controllers.js'
                ],
                dest: 'src/js/<%= pkg.name %>-main.js'
            },

        },
        uglify: {
            options: {
                mangle: true
            },
            my_target: {
                files: {
                    'src/js/<%= pkg.name %>-deps-min.js': ['src/js/<%= pkg.name %>-deps.js']
                }
            },
            my_target3: {
                files: {
                    'src/js/<%= pkg.name %>-main-min.js': ['src/js/<%= pkg.name %>-main.js']
                }
            },
        },
        connect: {
            'static': {
                options: {
                    hostname: 'localhost',
                    port: 9001,
                },
                proxies: [{
                    context: '/',
                    host: 'localhost',
                    port: 8001,
                    https: false,
                    xforward: false,

                }, ]
            },
            server: {
                options: {
                    context: '/',
                    hostname: "localhost",
                    port: 9000,
                    keepalive: true,
                    open: true,
                    middleware: function(connect, options) {
                        return [proxySnippet];
                    }
                },
                proxies: [{
                        context: ['/dboard', '/imdb'],
                        host: 'localhost',
                        port: 9200,

                    }, {
                        context: '/',
                        host: 'localhost',
                        port: 9001,

                    },


                ]
            },
        }

    });

    //npm tasks
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-connect-proxy');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-ngdocs');

    //tasks
    grunt.registerTask('default', 'Default Task Alias', ['build', 'ready']);
    grunt.registerTask('ready', 'Server Running', ['connect:static',
        'configureProxies:server',
        'connect:server'
    ]);
    grunt.registerTask('build', 'Build the application', [
        'concat',
        'uglify'
    ]);
}
