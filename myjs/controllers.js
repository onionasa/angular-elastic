angular.module('myApp').controller('mainCtr', ['$scope', 'querySrv', 'client', function($scope, querySrv, client) {
    $scope.isCollapsed = true;
    $scope.maxSize = 5;
    $scope.bigCurrentPage = 1;
    $scope.selectedSearch = false;
    $scope.filtered = false;


    $scope.pageChange = function(bigCurrentPage) {
        if ($scope.selectedSearch) {
            querySrv.generateQuery(false, $scope.selected, (bigCurrentPage - 1) * 25, 25);
        } else {
            querySrv.generateQuery(true, "", (bigCurrentPage - 1) * 25, 25);
        }

        client.search(function(res) {
            $scope.topList = res;
            $scope.bigTotalItems = res.hits.total;
        });
    }


    //// GENRE FILTER
    var genreList = [];
    querySrv.generateFilter("aggs", "tag", "Genre", "", "");
    querySrv.generateQuery(true, "", 0, 0);
    client.search(function(res) {
        for (var i = 0; i < res.aggregations.tag.buckets.length; i++) {
            var str = "{\"name\":\"" + res.aggregations.tag.buckets[i].key + "\"}";
            genreList.push(JSON.parse(str));
        };
        $scope.genreList = genreList;
    });
    querySrv.resetQuery();
    //// LANGUAGE FILTER
    var languageList = [];
    querySrv.generateFilter("aggs", "tag", "Country", "", "");
    querySrv.generateQuery(true, "", 0, 0);
    client.search(function(res) {
        for (var i = 0; i < res.aggregations.tag.buckets.length; i++) {
            var str = "{\"name\":\"" + res.aggregations.tag.buckets[i].key + "\"}";
            languageList.push(JSON.parse(str));
        };
        $scope.languageList = languageList;
    });
    querySrv.resetQuery();
    //// COUNTRY FILTER
    var countryList = [];
    querySrv.generateFilter("aggs", "tag", "Awards", "", "");
    querySrv.generateQuery(true, "", 0, 0);
    client.search(function(res) {
        for (var i = 0; i < res.aggregations.tag.buckets.length; i++) {
            var str = "{\"name\":\"" + res.aggregations.tag.buckets[i].key + "\"}";
            countryList.push(JSON.parse(str));
        };
        $scope.countryList = countryList;
    });
    querySrv.resetQuery();

    $scope.saveFilter = function() {
        $scope.bigCurrentPage = 1;
        querySrv.resetQuery();
        $scope.isCollapsed = true;
        $scope.filtered = true;
        querySrv.generateFilter("must", "range", "imdbVotes", $scope.imdb[0], $scope.imdb[1]);
        querySrv.generateFilter("must", "range", "Year", $scope.year[0], $scope.year[1]);
        querySrv.generateFilter("must", "term", "Genre", $scope.selectedGenre, "");
        querySrv.generateFilter("should", "term", "Awards", $scope.selectedCountry, "");
        querySrv.generateFilter("should", "term", "Country", $scope.selectedLanguage, "");
        if ($scope.selectedSearch) {
            querySrv.generateQuery(false, $scope.selected, ($scope.bigCurrentPage - 1) * 25, 25);
        } else {
            querySrv.generateQuery(true, "", ($scope.bigCurrentPage - 1) * 25, 25);
        }
        client.search(function(res) {
            $scope.topList = res;
            $scope.bigTotalItems = res.hits.total;
        });

    }

    $scope.resetFilter = function() {
        $scope.isCollapsed = true;
        $scope.filtered = false;
        querySrv.resetQuery();
        $scope.selectedCountry = [];
        $scope.selectedLanguage = [];
        $scope.selectedGenre = [];
        $scope.imdb = [0, 10];
        $scope.year = [1900, 2016];
    }


    querySrv.generateFilter("must", "range", "Poster", 1000, 7000);
    querySrv.generateFilter("sort", "", "imdbVotes", "desc", "max");
    querySrv.generateQuery(true, "", 0, 25);
    client.search(function(res) {
        $scope.topList = res;
        $scope.bigTotalItems = res.hits.total;
    });
    $scope.sortType = "desc";
    $scope.toggleSort = function() {
        if ($scope.sortType == "desc") {
            $scope.sortType = "asc";
        } else {
            $scope.sortType = "desc";
        }
    }
    $scope.sortBy = function(arg) {
        if ($scope.filtered && $scope.selectedSearch) {
            $scope.filtered = true;
            querySrv.resetSortFilter();
            querySrv.generateFilter("sort", "", arg, $scope.sortType, "avg");
            querySrv.generateQuery(false, $scope.selected, ($scope.bigCurrentPage - 1) * 25, 25);
        } else if ($scope.filtered && !$scope.selectedSearch) {
            querySrv.resetSortFilter();
            querySrv.generateFilter("sort", "", arg, $scope.sortType, "avg");
            querySrv.generateQuery(true, "", ($scope.bigCurrentPage - 1) * 25, 25);
        } else if (!$scope.filtered && $scope.selectedSearch) {
            querySrv.resetSortFilter();
            querySrv.generateFilter("sort", "", arg, $scope.sortType, "avg");
            querySrv.generateQuery(false, $scope.selected, ($scope.bigCurrentPage - 1) * 25, 25);
        } else {
            querySrv.resetSortFilter();
            querySrv.generateFilter("sort", "", arg, $scope.sortType, "avg");
            querySrv.generateQuery(true, "", ($scope.bigCurrentPage - 1) * 25, 25);
        }
        client.search(function(res) {
            $scope.topList = res;
            $scope.bigTotalItems = res.hits.total;
        });
    }


    $scope.sub = function() {
        $scope.bigCurrentPage = 1;
        $scope.selectedSearch = true;
        if ($scope.filtered) {
            $scope.filtered = true;
        } else {
            querySrv.resetQuery();
        }
        querySrv.generateQuery(false, $scope.selected, ($scope.bigCurrentPage - 1) * 25, 25);
        client.search(function(res) {
            $scope.topList = res;
            $scope.bigTotalItems = res.hits.total;
        });
    }
    $scope.label = function(item) {
        return item.Title + ' | Director: ' + item.Director + '| Released: ' + item.Released + ' | Cast: ' + item.Metacritic;
    };
    $scope.autoF = function() {
        if ($scope.filtered) {
            $scope.filtered = true;
        } else {
            querySrv.resetQuery();
        }
        querySrv.generateQuery(false, $scope.selected, 0, 10);
        client.search(function(res) {
            $scope.autos = $.map(res.hits, function(el) {
                return el;
            });
        });
    }

    $scope.red = function(arg) {
        window.location = "#/detail/" + arg
    }

}])

angular.module('myApp').controller('detailCtr', ['$scope', '$http', '$routeParams', function($scope, $http, $routeParams) {

    $http.get("http://localhost:9200/imdb/movies/" + $routeParams.id).success(function(res) {
        $scope.result = res;
    })

}])


angular.module('myApp').controller('statsCtr', ['$scope','toaster','tempSrv', 'dashboardSrv', '$modal', '$rootScope', 'querySrv', 'chartSrv', 'client', 'chartGenSrv',
    function($scope,toaster,tempSrv, dashboardSrv, $modal, $rootScope, querySrv, chartSrv, client, chartGenSrv) {
        $scope.chartModel = "Metacritic";
        $scope.chartTyp = "BarChart";
        $scope.panelColor = "default";
        $scope.panelSize = "col-md-4";


        $scope.open = function() {
            $rootScope.t = $modal.open({
                templateUrl: 'panel.html',
                windowClass: 'modal-default',
                controller: 'statsCtr',
                size: 'lg',
            });
        }




        $scope.sortableOptions = {
            'ui-floating': true,
            handle: '.myHandle',
        }

        if ($rootScope.dash.ready) {
            $rootScope.temp=[];
            var temp = $rootScope.dash._source.dash
            tempSrv.reset();
            tempSrv.setTemp($rootScope.dash._source.dash);


            for (var i = 0; i < temp.length; i++) {
                $rootScope.dash = [];
               
                var col = [];
                var chart = {};
                chartGenSrv.generate("aggs", "",
                    col, temp[i][0], temp[i][0], chart,
                    temp[i][1], "", "", "", "",temp[i],
                    function(dt,tp) {
                   console.log("asdasd"+tp)
                        $rootScope.charts2.push([tp[0], dt,
                           tp[2],
                            tp[3],
                            tp[4]
                        ]);

                        $rootScope.dash.push([tp[0], 
                            tp[1], tp[2], 
                            tp[3], tp[4]])
                    });
            };
            $scope.dama=$rootScope.dash;
        }

        $scope.charts = $rootScope.charts2;

        $scope.saveBoard = function(title, isDefault) {
            dashboardSrv.saveDboard(true, "masa");
            toaster.pop('success', "Dashboard Saved", "");
        }

        $scope.addChart = function(field, chartType, panelSize, panelColor, panelTitle) {
            if (!panelTitle) {
                panelTitle = "Untitled Panel"
            }
            var col = [];
            var chart = {};
            chartGenSrv.generate("aggs", "", col, field, field, chart,
                chartType, "", "", "", "","",
                function(dt) {
                    $rootScope.charts2.push([field, dt, panelSize, panelColor, panelTitle]);
                    $rootScope.dash.push([field, chartType, panelSize, panelColor, panelTitle]);

                });
            $rootScope.t.close();
        }

        $scope.removeChart = function(index) {
            console.log("index" + index);
            if (index > -1) {
                $scope.charts.splice(index, 1);
                $scope.dash.splice(index, 1);
            }
        }

    }
])
