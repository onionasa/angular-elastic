angular.module('myApp', ['ngRoute', 'toaster', 'ng-mfb', 'ui.bootstrap-slider', 'ui.bootstrap', 'isteven-multi-select', 'googlechart', 'ngAnimate', 'ui.sortable'])
    .config(['$routeProvider', function($routeProvider) {

        $routeProvider
            .when('/home', {
                templateUrl: 'src/views/home.html',
                controller: 'mainCtr'
            })
            .when("/search", {
                templateUrl: 'src/views/results.html',
                controller: "mainCtr"
            })
            .when('/detail/:id', {
                templateUrl: 'src/views/detail.html',
                controller: 'detailCtr'
            })
            .when('/stats', {
                templateUrl: 'src/views/stats.html',
                controller: 'statsCtr'
            })
            .otherwise({
                redirectTo: '/home'
            })

    }]).run(['$rootScope', 'dashboardSrv', '$http', function($rootScope, dashboardSrv, $http) {

        $http.get("http://localhost:9000/dboard/db/1").success(function(res) {
            $rootScope.dash = res;
            console.log(res._source);
            $rootScope.dash.ready = true;
        }).error(function(res) {
            $rootScope.dash = [];
            $rootScope.dash.ready = false;
            $http.post("http://localhost:9000/dboard").success(function(res) {
                var data = {
                    "mappings": {
                        "db": {
                            "_timestamp": {
                                "enabled": "true",
                                "store": "yes"
                            }
                        }
                    }
                }
            })

        })


        $rootScope.charts2 = [];
    }])
