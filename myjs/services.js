var myapp = angular.module('myApp');


myapp.service('querySrv', [function() {
    var myQuery = [];
    var mustFilter = [];
    var mustNotFilter = [];
    var shouldFilter = [];
    var sortByFilter = [];
    var aggsFilter = [];
    return {
        generateFilter: function(filter_type, query_type, field, dt1, dt2) {
            if (filter_type == "must") {

                if (query_type == "term") {
                    if (dt1.length > 0) {
                        for (var i = 0; i < dt1.length; i++) {
                            var str = "{\"" + query_type + "\":{\"" + field + "\":\"" + dt1[i].name + "\"}}";
                            mustFilter.push(JSON.parse(str));
                        }
                    }
                } else if (query_type == "range") {
                    var str = "{\"" + query_type + "\":{\"" + field + "\":{\"gte\":" + dt1 + ",\"lte\":" + dt2 + "}}}";
                    mustFilter.push(JSON.parse(str));
                }


            } else if (filter_type == "should") {
                if (query_type == "term") {
                    if (dt1.length > 0) {
                        for (var i = 0; i < dt1.length; i++) {
                            var str = "{\"" + query_type + "\":{\"" + field + "\":\"" + dt1[i].name + "\"}}";
                            shouldFilter.push(JSON.parse(str));
                        }
                    }
                } else if (query_type == "range") {
                    var str = "{\"" + query_type + "\":{\"" + field + "\":{\"gte\":" + dt1 + ",\"lte\":" + dt2 + "}}}";
                    shouldFilter.push(JSON.parse(str));
                }
            } else if (filter_type == "must_not") {
                if (query_type == "term") {
                    if (dt1.length > 0) {
                        for (var i = 0; i < dt1.length; i++) {
                            var str = "{\"" + query_type + "\":{\"" + field + "\":\"" + dt1[i].name + "\"}}";
                            mustNotFilter.push(JSON.parse(str));
                        }
                    }
                } else if (query_type == "range") {
                    var str = "{\"" + query_type + "\":{\"" + field + "\":{\"gte\":" + dt1 + ",\"lte\":" + dt2 + "}}}";
                    mustNotFilter.push(JSON.parse(str));
                }
            } else if (filter_type == "sort") {
                var str = "{\"" + field + "\":{\"order\":\"" + dt1 + "\",\"mode\":\"" + dt2 + "\"}}";
                sortByFilter.push(JSON.parse(str));
            } else if (filter_type == "aggs") {
                var str = "{\"" + query_type + "\":{\"terms\":{\"field\":\"" + field + "\"}}}";
                aggsFilter.push(JSON.parse(str));
            } else if (filter_type == "hist") {
                var str = "{\"" + query_type + "\":{\"histogram\":{\"field\":\"" + field + "\",\"interval\":" + dt1 + "}}}";
                aggsFilter.push(JSON.parse(str));
            }

        },
        generateQuery: function(filter, data, pg, sz) {
            myQuery = [];
            var aggs;
            if (aggsFilter.length > 0) {
                aggs = aggsFilter[0]
            } else {
                aggs = {}
            }
            if (filter) {
                myQuery.push({

                    "query": {
                        "match_all": {

                        }
                    },
                    "from": pg,
                    "size": sz,
                    "sort": sortByFilter,
                    "filter": {
                        "bool": {
                            "must": mustFilter,
                            "must_not": mustNotFilter,
                            "should": shouldFilter

                        }
                    },
                    "aggs": aggs
                })
            } else {
                myQuery.push({

                    "query": {
                        "match": {
                            "_all": {
                                "query": data,
                                "operator": "and"
                            }
                        }
                    },
                    "from": pg,
                    "size": 50,
                    "sort": sortByFilter,
                    "filter": {
                        "bool": {
                            "must": mustFilter,
                            "must_not": mustNotFilter,
                            "should": shouldFilter

                        }
                    },
                    "aggs": aggs
                })
            }

        },
        getQuery: function() {
            return myQuery;
        },
        resetSortFilter: function() {
            sortByFilter = [];
        },
        resetQuery: function() {
            myQuery = [];
            mustFilter = [];
            mustNotFilter = [];
            shouldFilter = [];
            sortByFilter = [];
            aggsFilter = [];
        }
    }
}]);

myapp.service('client', ['$http', 'querySrv', '$rootScope', function($http, querySrv, $rootScope) {

    return {
        search: function(callback) {
            $rootScope.serviceActive = true;
            $http.post("http://localhost:9000/imdb/_search", querySrv.getQuery()[0]).success(function(res) {
                callback(res);
                $rootScope.serviceActive = false;
            })
        }
    }

}])



angular.module('myApp').service('chartSrv', function() {
    return {
        generateCol: function(arg, arg2) {
            arg.push(arg2);
        },
        generateRow: function(arg, arg2) {
            arg.push(arg2);
        },
        generateChart: function(chart1, arg, cType, cStyle, cTtt, hTtt, vTtt) {
            chartType = cType;
            cssStyle = cStyle;
            cTitle = cTtt;
            hTitle = hTtt;
            vTitle = vTtt;
            chart1.type = chartType;
            chart1.cssStyle = cssStyle;
            chart1.data = arg;
            chart1.options = {
                "title": cTitle,
                "isStacked": true,
                "fill": 20,
                "displayExactValues": true,
                "vAxis": {
                    "title": vTitle,
                    "gridlines": {
                        "count": 6
                    }
                },
                "hAxis": {
                    "title": hTitle,

                }
            };

            chart1.formatters = {};

            return chart1;
        }


    }

})


angular.module('myApp').service('chartGenSrv', ['client', 'querySrv', 'chartSrv', function(client, querySrv, chartSrv) {
    return {
        generate: function(aggs_type, interval, col, label, field, 
            chart, chartType, cssStyle, cTitle, vTitle, hTitle, args,callback) {
            querySrv.resetQuery();
            chartSrv.generateCol(col, ["Country", label]);
            querySrv.generateFilter(aggs_type, "tag", field, interval, "");
            querySrv.generateQuery(true, "", 0, 0);
            client.search(function(res) {
                for (var i = 0; i < res.aggregations.tag.buckets.length; i++) {
                    if (res.aggregations.tag.buckets[i].key.toString() !== "") {
                        chartSrv.generateRow(col, [res.aggregations.tag.buckets[i].key.toString(),
                            parseInt(res.aggregations.tag.buckets[i].doc_count)
                        ]);
                    }

                };

                querySrv.resetQuery();
                callback(chartSrv.generateChart(chart, col, chartType, cssStyle, cTitle, vTitle, hTitle),args);
            });
        }
    }
}])


angular.module('myApp').service('dashboardSrv', ['$http', '$rootScope', function($http, $rootScope) {
    var dboard = {};

    return {
        saveDboard: function(isDefault, title) {
            var query = {
                "size": 1,
                "query": {
                    "match_all": {}
                },
                "sort": [{
                    "_timestamp": {
                        "order": "desc"
                    }
                }]
            }
            dboard = {
                "isDefault": isDefault,
                "title": title,
                "dash": $rootScope.dash
            }
            $http.get("http://localhost:9000/dboard/_search", query).success(function(res) {
                if (parseInt(res.hits.total) == 0) {
                    console.log("bla"+dboard.isDefault)
                    $http.post("http://localhost:9000/dboard/db/1", dboard).success(function(res) {})
                } else {
                    if (isDefault) {
                        console.log("bla"+dboard.isDefault)
                        $http.post("http://localhost:9000/dboard/db/1", dboard).success(function(res) {})
                    } else {
                        $http.post("http://localhost:9000/dboard/db/" + (parseInt(res.hits.hits[0]._id) + 1), dboard).success(function(res) {})
                    }
                }

            })
        },
        getDefaultDboard: function(callback) {
            $http.get("http://localhost:9000/dboard/db/1").success(function(res) {
                callback(res);
            })
        },
        getDboard: function(id, callback) {
            $http.get("http://localhost:9000/dboard/db/" + id).success(function(res) {
                callback(res);
            })
        }
    }
}])


angular.module('myApp').service('tempSrv', ['$http', function($http){
    var temp;
    return{
        setTemp:function(arg){
            temp=arg;
        },
        getTemp:function(){
            return temp;
        },
        reset:function(){
            temp=[];
        }
    }
}])